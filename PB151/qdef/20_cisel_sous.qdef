V polyadické soustavě je číslo
 :r1 součet bitů n-tice, ve které je uloženo.
 :r2 vždy dělitelné svým základem.
 :r3 součet mocnin základu vynásobených číslicemi.
:r3 ok
--
Čísla lze snadno (každou k-tici číslic nižší soustavy nahradíme číslicí soustavy vyšší) převádět mezi soustavami o základu
 :r1 5 a 7
 :r2 8 a 2
 :r3 10 a 16
:r2 ok
--
Číslo 21 v desítkové soustavě po převedení do soustavy dvojkové je
 :r1 10101
 :r2 11011
 :r3 10011
 :r4 nelze do dvojkové soustavy převést
:r1 ok
--
Pascalovský typ INTEGER je celé číslo, které se na počítačích PC zobrazuje v
 :r1 přímém kódu.
 :r2 doplňkovém kódu.
 :r3 inverzním kódu.
:r2 ok
--
Znaménkový bit v celém čísle je zpravidla bit
 :r1 nejnižšího řádu.
 :r2 nultého řádu.
 :r3 nejvyššího řádu.
:r3 ok
--
Znaménkový bit bývá zpravidla
 :r1 roven jedné, pokud se zobrazuje číslo kladné
 :r2 roven nule, pokud se zobrazuje číslo záporné
 :r3 roven nule, pokud se zobrazuje číslo kladné
:r3 ok
--
Rozsah zobrazení celého čísla uloženého ve dvojkovém doplňkovém kódu na 8 (celkem) bitech je
 :r1 <-128;127>
 :r2 <-256;255>
 :r3 <-511;512>
 :r4 <-1024;1023>
 :r5 žádný z uvedených
:r1 ok
--
Největší zobrazitelné celé číslo ve dvojkovém doplňkovém kódu má tvar
 :r1 100...00
 :r2 111...11
 :r3 000...00
 :r4 100...01
 :r5 011...11
:r5 ok
--
Při sčítání dvou čísel v inverzním kódu jako korekci výsledku použijeme:
 :r1 násobný přenos
 :r2 kruhový přenos
 :r3 konverzní přenos
 :r4 desítkový přenos
:r2 ok
--
Přeplnění (přetečení) je stav, ve kterém
 :r1 výsledek spadá mimo přesnost
 :r2 výsledek spadá mimo rozlišitelnost
 :r3 výsledek spadá mimo rozsah zobrazení
:r3 ok
--
Vyberte <B>ne</B>pravdivé tvrzení týkající se zobrazení celého čísla:
 :r1 přímý kód obsahuje kladnou a zápornou nulu
 :r2 inverzní kód obsahuje kladnou a zápornou nulu
 :r3 doplňkový kód obsahuje pouze jednu nulu
 :r4 rozsah zobrazení doplňkového kódu je symetrický
 :r5 se všemi bity doplňkového kódu se pracuje stejně
:r4 ok
--
Inverzní kód pro zobrazení celého čísla <B>ne</B>má
 :r1 jednu nulu
 :r2 symetrický rozsah zobrazení
 :r3 znaménkový bit
 :r4 ve znaménkovém bitu jedničku pro označení záporného čísla
:r1 ok
--
Znaménkový bit pro zobrazení celého čísla
 :r1 je bit nejnižšího řádu
 :r2 se běžně nepoužívá
 :r3 je bit nejnižšího řádu pouze pokud se jedná o číslo
 :r4 má hodnotu 1 pro kladné číslo
 :r5 má hodnotu 0 pro kladné číslo
:r5 ok
--
Přetečení v celočíselné aritmetice ve dvojkovém doplňkovém kódu nastane
 :r1 pokud se přenos ze znaménkového bitu rovná přenosu do znaménkového bitu
 :r2 pokud se přenos ze znaménkového bitu nerovná přenosu do znaménkového bitu
 :r3 pokud se přenos ze znaménkového bitu nerovná znaménkovému bitu
 :r4 pokud se přenos ze znaménkového bitu rovná znaménkovému bitu
 :r5 pokud výsledek operace nespadá mimo rozsah zobrazení
:r2 ok
--
Osmičkovou a šestnáctkovou soustavu používáme, protože:
 :r1 vnitřně si počítač uchovává data v těchto soustavách
 :r2 výpočet procesoru je rychlejší než při použití dvojkové soustavy
 :r3 zápis čísla je kratší než ve dvojkové soustavě
 :r4 vstupní a výstupní zařízení pracují s těmito soustavami
:r3 ok
--
Binární hodnota 0,1001 odpovídá dekadické hodnotě desetinného čísla:
 :r1 9/16
 :r2 1/32
 :r3 9/10
 :r4 1/16
 :r5 10/9
:r1 ok
--
Při sčítání ve dvojkovém doplňkovém kódu platí:
 :r1 přetečení nastane, pokud je rozsah zobrazení jiný než &lt;0;2<SUP>n</SUP>-1&gt;
 :r2 všechny bity (kromě znaménkového) se sčítají stejně
 :r3 vznikne-li přenos ze znaménkového bitu, je nutné provádět tzv. kruhový přenos
 :r4 přetečení nastane, pokud se přenosy z/do znaménkového bitu rovnají
 :r5 vznikne-li přenos ze znaménkového bitu, tak se ignoruje
:r5 ok
--
Dvojkové číslo 1000 v přímém kódu v zobrazení se znaménkem na 4 bitech je:
 :r1 největší zobrazitelné
 :r2 nejmenší zobrazitelné
 :r3 kladná nula
 :r4 záporná nula
 :r5 žádná odpověď není správná
:r4 ok
--
Dvojkové číslo 1000 v inverzním kódu v zobrazení se znaménkem na 4 bitech je:
 :r1 největší zobrazitelné
 :r2 nejmenší zobrazitelné
 :r3 kladná nula
 :r4 záporná nula
 :r5 žádná odpověď není správná
:r2 ok
--
Dvojkové číslo 1111 v doplňkovém kódu v zobrazení se znaménkem na 4 bitech je:
 :r1 největší zobrazitelné
 :r2 nejmenší zobrazitelné
 :r3 kladná nula
 :r4 záporná nula
 :r5 žádná odpověď není správná
:r5 ok
--
Kruhový přenos je:
 :r1 inverze bitů
 :r2 inverze bitů a přičtení jedničky k výsledku
 :r3 přičtení přenosu z nejvyššího řádu k výsledku
 :r4 přičtení přenosu z nejvyššího řádu ke znaménkovému bitu
 :r5 přičtení jedničky k nejvyššímu řádu výsledku
:r3 ok
--
Kladná čísla v zobrazení se znaménkem mají na n bitech:
 :r1 ve všech kódech stejný rozsah
 :r2 v přímém kódu o 1 větší rozsah než v inverzním
 :r3 stejný rozsah jako kladná čísla v zobrazení bez znaménka
 :r4 v inverzním kódu o 1 číslo méně, než je záporných
 :r5 v inverzním kódu rozsah &lt;0;2<SUP>n</SUP>-1&gt;
:r1 ok
--
Které z dvojkových čísel v reprezentaci se znaménkem na 4 bitech je kladné?
 :r1 1010 v inverzním kódu
 :r2 0100 v inverzním kódu
 :r3 1010 v přímém kódu
 :r4 1111 v doplňkovém kódu
 :r5 všechny odpovědi jsou správné
:r2 ok
--
Kladná čísla v reprezentaci bez znaménka mají na n bitech rozsah:
 :r1 &lt;0;2<SUP>n</SUP>-1&gt;
 :r2 &lt;0;2<SUP>n-1</SUP>-1&gt;
 :r3 &lt;0;2<SUP>n-1</SUP>+1&gt;
 :r4 &lt;-2<SUP>n</SUP>-1;2<SUP>n</SUP>-1&gt;
 :r5 &lt;-2<SUP>n-1</SUP>-1;2<SUP>n-1</SUP>-1&gt;
:r1 ok
--
Rozsah zobrazení směrem ke kladným číslům a směrem k záporným číslům je rozložen asymetricky v:
 :r1 přímém kódu
 :r2 inverzním kódu
 :r3 doplňkovém kódu
 :r4 přímém a inverzním kódu
 :r5 inverzním a doplňkovém kódu
:r3 ok
--
Dvě reprezentace nuly se vyskytují v:
 :r1 přímém a doplňkovém kódu
 :r2 přímém a inverzním kódu
 :r3 inverzním a doplňkovém kódu
 :r4 doplňkovém, inverzním a přímém kódu
:r2 ok
--
Která z čísel jsou shodná (nejvyšší bit je znaménkový)?
 :r1 1001 v přímém a 1010 v inverzním kódu
 :r2 1101 v inverzním a 1110 v doplňkovém kódu
 :r3 1111 v doplňkovém a 1000 v přímém kódu
 :r4 1000 v doplňkovém a 1000 v inverzním kódu
 :r5 žádná z odpovědí není správná
:r2 ok
--
Rozsah zobrazení dvojkového doplňkového kódu na n bitech je:
 :r1 &lt;0;2<SUP>n-1</SUP>-1&gt;
 :r2 &lt;-2<SUP>n-1</SUP>;2<SUP>n-1</SUP>-1&gt;
 :r3 &lt;-2<SUP>n-1</SUP>-1;2<SUP>n-1</SUP>-1&gt;
 :r4 &lt;-2<SUP>n</SUP>-1;2<SUP>n</SUP>-1&gt;
 :r5 &lt;-2<SUP>n</SUP>+1;2<SUP>n</SUP>-1&gt;
:r2 ok
--
Dvojkové číslo 1001 v reprezentaci se znaménkem na 4 bitech se v inverzním kódu rovná
 :r1 6
 :r2 -6
 :r3 9
 :r4 -9
:r2 ok
--
Jak při sčítání binárních čísel ve dvojkovém doplňkovém kódu poznám, že došlo k přetečení?
 :r1 k přetečení nemůže dojít, zabraňuje mu kruhový přenos
 :r2 přenos ze znaménkového bitu je 1 
 :r3 přenos do znaménkového bitu se nerovná přenosu ze znaménkového bitu
 :r4 přenos do znaménkového bitu se rovná přenosu ze znaménkového bitu
:r3 ok
--
Číslo 14 v decimální soustavě odpovídá
 :r1 D v hexadecimální soustavě
 :r2 15 v oktalové soustavě
 :r3 1101 v binární soustavě
 :r4 E v hexadecimální soustavě
:r4 ok
--
Kruhový přenos v inverzním kódu se využívá
 :r1 pro korekci při přechodu přes nulu
 :r2 pro zkopírování nejnižšího bitu do nejvyššího
 :r3 pro zkopírování nejvyššího bitu do nejnižšího
 :r4 kruhový přenos se v inverzním kódu nepoužívá
:r1 ok
--
Jednoduše nelze převádět čísla mezi soustavami o základech
 :r1 5 a 25
 :r2 3 a 9
 :r3 4 a 40
 :r4 6 a 216
 :r5 6 a 36
:r3 ok
--
Osmičková soustava se také nazývá
 :r1 oktetová
 :r2 oktalová
 :r3 oktanová
 :r4 oktarová
 :r5 oklotová
:r2 ok
